/*const express = require('express'),
    app = express(),
    port = process.env.PORT || 3000;

app.listen(port);    
console.log('book list RESTful API server started on port : ', port);
*/


const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const mongoose = require('mongoose');

const uri = 'mongodb://localhost/Bookdb';
const Book = require('./api/models/bookListModel'); // created model loading

// mongoose instance connection url connection
mongoose.connect(uri, {
    useNewUrlParser: true,
    // useCreateIndex: true,
    useUnifiedTopology: true
}).then(() => {
    console.info("Connexion à la base de données réussie !");
}).catch(err => {
    console.error("Erreur lors de la connexion à la base de données :", err.message);
});

app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());

const routes = require('./api/routes/bookListRoutes');      // Importation des routes
routes(app);                                                // Enregistrement des routes

app.get('*', (req, res) => {
    res.status(404).send({
        url: req.originalUrl + ' not found'
    });
});

app.listen(port, () => {
    console.log(`API server listening on port ${port}`);
});