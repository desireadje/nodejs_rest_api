'use strict';

// Get Book Manager
const mongoose = require('mongoose'),
Book = mongoose.model('Book');

exports.list_all_books = (request, response) => {
    Book.find().then((err, result) => {
        if (err) return response.status(500).send(err);
        response.status(200).send(result)
    });
};

exports.create_a_book = (request, response) => {
    console.log("=====> request.body <====== " + JSON.stringify(request.body));
    let book = new Book(request.body);
    book.save((err, result) => {
        if (err) return response.status(500).send(err);
        response.json(result);
    });
};

exports.read_a_book = (request, response) => {
    Book.findById(request.params.bookId, (err, result) => {
        if (err) return response.status(500).send(err);
        res.json(result);
    });
};

/*
exports.update_a_book = (request, response) => {
    Book.findOneAndUpdate({
        _id: request.params.bookId
    }, request.body, {
        new: true
    }, (err, task) => {
        if (err)
            response.send(err);
            response.json(book);
    });
};
*/
exports.update_a_book = (request, response) => {
    Book.findOneAndUpdate({_id: req.params.bookId}, request.body, {new: true}, (err, task) => {
        if (err) return response.status(500).send(err);
        response.json(book);
     });
   };

exports.delete_a_book = (req, res) => {
    Book.remove({
        _id: req.params.bookId
    }, (err, book) => {
        if (err)
            res.send(err);
        res.json({
            message: 'Book successfully deleted'
        });
    });
};