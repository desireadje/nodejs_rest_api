'use strict';
module.exports = (app) => {
    const booksRoute = require('../controllers/bookListController');  // bookList Routes (booksRoute)
    app.route('/books')
        .get(booksRoute.list_all_books)
        .post(booksRoute.create_a_book);
        
    app.route('/books/:bookId')
        .get(booksRoute.read_a_book)
        .put(booksRoute.update_a_book)
        .delete(booksRoute.delete_a_book);
};