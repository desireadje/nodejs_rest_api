'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BookSchema = new Schema({
    name: {
      type: String,
      required: 'Enter book name'
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    status: {
        type: [{
            type: String,
            enum: ['pending', 'ongoing', 'completed']
        }],
        default: ['pending'],
        required: 'Select boot status'
    }
});
module.exports = mongoose.model('Book', BookSchema);